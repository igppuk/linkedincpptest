// LinkedInTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "FileSorter.h"

#include "gtest/gtest.h"

TEST(TestCaseName, TestName) {
{
	auto sorter = Sorter("d:/readme(phones+accounts).txt");

	const auto lines = sorter.Action("my_projects");
	EXPECT_NE(0, static_cast<int>(lines.size()));

	const bool result = Sorter::Flush(lines, ":/readme(phones+accounts)_new.txt");
	EXPECT_TRUE(result);
}

int main(int argc, char** argv)
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();

	//auto sorter = Sorter("d:/Install/Sony Vegas 13 x64/���������.TXT");
	//const auto lines = sorter.Action("����������");
	//Sorter::Flush(lines, "d:/Install/Sony Vegas 13 x64/���������_1.TXT");
}