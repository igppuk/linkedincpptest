#pragma once
#include <string>
#include <vector>

class Sorter
{
public:
	explicit Sorter(const std::string & fileName);
	virtual ~Sorter();

	std::vector<std::string> & Action(const std::string & removeWord = "");
	static bool Flush(const std::vector<std::string> & lines, const std::string & fileName);

protected:
	std::string _fileName;
	std::vector<std::string> _result;
};

