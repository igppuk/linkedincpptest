#include "pch.h"
#include "FileSorter.h"
#include <fstream>
#include <regex>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <cctype>

std::string lowercase(std::string s)
{
	std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c) {
		return std::tolower(c);
		});
	return s;
}

Sorter::Sorter(const std::string& fileName)
{
	this->_fileName = fileName;
}

Sorter::~Sorter() = default;

std::vector<std::string> & Sorter::Action(const std::string & removeWord)
{
	this->_result.clear();

	std::ifstream infile(this->_fileName);
	if (infile.is_open()) {

		std::string line;
		const std::regex e(removeWord, std::regex::icase);

		while (std::getline(infile, line))
		{
			// removing the word
			std::string result;
			std::regex_replace(std::back_inserter(result), line.begin(), line.end(), e, "");

			// sorting by words
			std::istringstream iss(result);
			std::vector<std::string> split(std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>());

			std::sort(split.begin(), split.end(), [](const std::string & first, const std::string & second) {
				return lowercase(first) < lowercase(second);
			});

			// combining the string
			std::string r;
			for (const auto & s : split)
			{
				r.append(s + " ");
			}

			if (r.length() > 0)
			{
				this->_result.push_back(r);
			}
		}

		infile.close();
	}

	return this->_result;
}

bool Sorter::Flush(const std::vector<std::string> & lines, const std::string & fileName)
{
	auto result = false;

	std::ofstream outfile(fileName, std::ios::out);
	if (outfile.is_open())
	{
		for (const auto & line : lines)
		{
			outfile << line << "\r\n";
		}

		result = true;
		outfile.close();
	}

	return result;
}
